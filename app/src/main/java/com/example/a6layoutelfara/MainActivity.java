package com.example.a6layoutelfara;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button explicitIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        explicitIntent = (Button)findViewById(R.id.explicitIntent1);
        explicitIntent.setOnClickListener((View.OnClickListener) this);


    }
    @Override
    public void onClick(View v){


        Intent explicitIntent = new Intent(MainActivity.this, IntentActivity.class);
        startActivity(explicitIntent);



    }
}
